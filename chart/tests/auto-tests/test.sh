#!/bin/bash -v

RED='\033[0;31m'
GREEN='\033[1;32m'
YELLOW='\e[1;33m'

COUNTER=0
AMMOUNT=0
declare -a failed=()

printf "${YELLOW}RABBITMQ TESTS:\n"
rabbitmqstatus=$(helm status rabbitmq)
rabbitmqall=$(helm get values rabbitmq -a)

let AMMOUNT=AMMOUNT+1
if [[ $rabbitmqstatus = *'STATUS: deployed'* ]]; then 
   printf "${GREEN}Rabbitmq DEPLOYED\n"
   let COUNTER=COUNTER+1
   else {
   	failed+=('ERROR: Rabbirmq NOT DEPLOYED')
   	}
fi

let AMMOUNT=AMMOUNT+1
if [[ $rabbitmqall = *'type: ClusterIP'* ]]; then 
   printf "${GREEN}Type is ClusterIP\n"
   let COUNTER=COUNTER+1
   else {
   	failed+=('ERROR: Unknown type')
   	}
fi

let AMMOUNT=AMMOUNT+1
if [[ $rabbitmqall = *'distPort: 25672'* ]]; then 
   printf "${GREEN}Dist port is 25672\n"
   let COUNTER=COUNTER+1
   else {
   	failed+=('ERROR: Dist port is not 25672')
   	}
fi

let AMMOUNT=AMMOUNT+1
if [[ $rabbitmqall = *'managerPort: 15672'* ]]; then 
   printf "${GREEN}Manager port is 15672\n"
   let COUNTER=COUNTER+1
   else {
   	failed+=('ERROR: Manager port is not 15672')
   	}
fi

let AMMOUNT=AMMOUNT+1
if [[ $rabbitmqall = *'metricsPort: 9419'* ]]; then 
   printf "${GREEN}Metrics port is 9419\n"
   let COUNTER=COUNTER+1
   else {
   	failed+=('ERROR: Metrics port is not 9419')
   	}
fi   	

let AMMOUNT=AMMOUNT+1
if [[ $rabbitmqall = *'port: 5672'* ]]; then 
   printf "${GREEN}Port is 5672\n"
   let COUNTER=COUNTER+1
   else {
   	failed+=('ERROR: Port is not 5672')
   	}
fi

let AMMOUNT=AMMOUNT+1
if [[ $rabbitmqall = *'tlsPort: 5671'* ]]; then 
   printf "${GREEN}Tls port is 5671\n"
   let COUNTER=COUNTER+1
   else {
   	failed+=('ERROR: Tls port is not 5671')
   	}
fi

FAIL=$(( AMMOUNT - COUNTER ))

if [[ $COUNTER = $AMMOUNT ]]; then
   printf "${GREEN}${COUNTER}/${AMMOUNT} TESTS PASSED SUCCSESS\n"
   else {
   	printf "${RED}${FAIL}/${AMMOUNT} TESTS FAILED\n"
	for each in "${failed[@]}"
	do
  	  echo "$each"
	done
   	}
fi
